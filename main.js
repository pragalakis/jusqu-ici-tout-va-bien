const minHeight = 700;
window.onload = function() {
  if (window.innerHeight > minHeight) {
    cloneBuilding();
  }
};

let buildingHeight = 255;
window.onscroll = function(e) {
  if (e.pageY === undefined) {
    e = { pageY: window.pageYOffset };
  }

  if (e.pageY >= buildingHeight) {
    cloneBuilding();
    buildingHeight += 345;
  }
};

let id = 1;
function cloneBuilding() {
  let walls = document.getElementById('walls');
  let clone = walls.cloneNode(true);
  walls.id = `walls-${++id}`;
  walls.parentNode.appendChild(clone);
}
