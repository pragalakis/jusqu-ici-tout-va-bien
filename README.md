# jusqu-ici-tout-va-bien

### [pragalakis.gitlab.io/jusqu-ici-tout-va-bien](https://pragalakis.gitlab.io/jusqu-ici-tout-va-bien)

#### Jusqu' ici tout va bien  

C’est l’histoire d’un homme qui tombe d’un immeuble de cinquante étages. Le mec, au fur et à mesure de sa chute, il se répète sans cesse pour se rassurer : jusqu’ici tout va bien, jusqu’ici tout va bien, jusqu’ici tout va bien.
Mais l'important n’est pas la chute, c’est l’atterrissage."

Hubert Koundé, La Haine (1995), écrit par Mathieu Kassovitz


#### So far so good

"This is the story of a man who falls from a fifty-story building. On his way down past each floor, he kept saying to reassure himself: So far so good... so far so good... so far so good.
But the important thing is not the fall, it's the landing."

Hubert Koundé, La Haine (1995), written by Mathieu Kassovitz

![](screenshot.png)
